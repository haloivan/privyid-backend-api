'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('userBalanceHistories', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userBalanceId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: "userBalances",
          key: "id"
        }
      },
      balanceBefore: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      balanceAfter: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      activity: {
        allowNull: false,
        type: Sequelize.STRING
      },
      type: {
        allowNull: false,
        type: Sequelize.ENUM('debit', 'kredit')
      },
      ip: {
        allowNull: false,
        type: Sequelize.STRING
      },
      location: {
        allowNull: false,
        type: Sequelize.STRING
      },
      userAgent: {
        allowNull: false,
        type: Sequelize.STRING
      },
      author: {
        allowNull: false,
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('userBalanceHistories');
  }
};