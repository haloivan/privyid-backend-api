const db = require('../models')

module.exports = async (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1]
    const isBlacklisted = await db.tokens.findOne({ raw: true, where: { blacklisted: token } })
    if (isBlacklisted) {
        res.sendStatus(401)
    }
    next()
}