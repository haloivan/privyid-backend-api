'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class bankBalanceHistories extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  bankBalanceHistories.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    bankBalanceId: {
      allowNull: false,
      type: DataTypes.INTEGER,
      references: {
        model: "bankBalances",
        key: "id"
      }
    },
    balanceBefore: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    balanceAfter: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    activity: {
      allowNull: false,
      type: DataTypes.STRING
    },
    type: {
      allowNull: false,
      type: DataTypes.ENUM('debit', 'kredit')
    },
    ip: {
      allowNull: false,
      type: DataTypes.STRING
    },
    location: {
      allowNull: false,
      type: DataTypes.STRING
    },
    userAgent: {
      allowNull: false,
      type: DataTypes.STRING
    },
    author: {
      allowNull: false,
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'bankBalanceHistories',
  });
  return bankBalanceHistories;
};