'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class userBalanceHistories extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.userBalances)
    }
  };
  userBalanceHistories.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    userBalanceId: {
      allowNull: false,
      type: DataTypes.INTEGER,
      references: {
        model: "userBalances",
        key: "id"
      }
    },
    balanceBefore: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    balanceAfter: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    activity: {
      allowNull: false,
      type: DataTypes.STRING
    },
    type: {
      allowNull: false,
      type: DataTypes.ENUM('debit', 'kredit')
    },
    ip: {
      allowNull: false,
      type: DataTypes.STRING
    },
    location: {
      allowNull: false,
      type: DataTypes.STRING
    },
    userAgent: {
      allowNull: false,
      type: DataTypes.STRING
    },
    author: {
      allowNull: false,
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'userBalanceHistories',
  });
  return userBalanceHistories;
};