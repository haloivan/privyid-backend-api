'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class bankBalances extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  bankBalances.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    balance: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    balanceAchieve: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    code: {
      allowNull: false,
      type: DataTypes.STRING
    },
    enable: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    }
  }, {
    sequelize,
    modelName: 'bankBalances',
  });
  return bankBalances;
};