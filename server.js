require('dotenv').config()
const express = require('express')
const cors = require('cors')
const readDir = require('read-dir-deep');
const path = require('path')
const routesPath = path.resolve('routes')
const filePaths = readDir.readDirDeepSync(routesPath)


const app = express()
app.use(express.json())

//** CORS OPTION */
const corsOptionsDelegate = function (req, callback) {
    callback(null, { origin: true })
}
app.use(cors(corsOptionsDelegate))

filePaths.forEach((filePath) => {
    const relativeFilePath = `./${filePath}`
    const route = require(relativeFilePath)
    app.use(route)
})


const port = (process.env.PORT) ? process.env.PORT : 3000;
const hostname = (process.env.HOSTNAME == "localhost") ? `http://localhost:${port}` : process.env.HOSTNAME;
app.listen(port, () => {
    console.log(`The App start and listening in ${hostname}`);
})
