const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = {
    encryptPass: function (password) {
        return new Promise((resolve, reject) => {
            bcrypt.hash(password, saltRounds, function (err, hash) {
                if (err)
                    reject(err)
                else
                    resolve(hash)
            });
        })
    },
    decryptPass: function (password, hash) {
        return new Promise((resolve, reject) => {
            bcrypt.compare(password, hash, function (err, result) {
                if (err)
                    reject(err)
                else
                    resolve(result)
            });
        })
    }
}
