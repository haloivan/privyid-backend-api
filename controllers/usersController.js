const cryptHelper = require("../helpers/cryptHelper");
const jwt = require('jsonwebtoken')
const db = require('../models');

module.exports = {
    login: async function (req, res) {
        //** validating req body */
        if (!Object.keys(req.body).length) {
            return res.sendStatus(400)
        }

        let { loginMethod, password } = req.body

        //** validating login method*/
        if (!loginMethod) {
            return res.status(406).send("loginMethod in body needed")
        }
        if (typeof loginMethod != "string") {
            return res.status(406).send("loginMethod not string")
        }

        //** validating password format and encrypting */
        if (!password) {
            return res.status(406).send("password in body needed")
        }
        if (typeof password != "string") {
            return res.status(406).send("password not string")
        }
        if (password.length < 8) {
            return res.status(406).send("password minimum is 8 character")
        }
        if (password.length > 12) {
            return res.status(406).send("password maximum is 12 character")
        }

        //** check loginMethod is email or username and check user data in database */
        const isEmail = loginMethod.includes('@')

        let user;
        if (isEmail) {
            user = await db.users.findOne({ raw: true, where: { email: loginMethod } })
        }
        user = await db.users.findOne({ raw: true, where: { username: loginMethod } })
        if (!user) {
            return res.status(404).send("user not found")
        }

        //** check password is match or not */
        if (!await cryptHelper.decryptPass(password, user.password)) {
            return res.status(406).send("wrong password")
        }

        //** create tokenization data */
        const userData = { id: user.id, username: user.username, email: user.email }
        userData.token = jwt.sign(userData, process.env.JWT_SECRET_TOKEN, {
            expiresIn: '1h'
        })

        //** return response if all process is ok*/
        res.status(200).json(userData)

    },
    register: async function (req, res) {
        //** validating req body */
        if (!Object.keys(req.body).length) {
            return res.sendStatus(400)
        }

        let { username, email, password } = req.body
        //** validating username */
        if (!username) {
            return res.status(406).send("username in body needed")
        }
        if (typeof username != "string") {
            return res.status(406).send("username not string")
        }

        //** validating email */
        if (!email) {
            return res.status(406).send("email in body needed")
        }
        if (typeof email != "string" && (!email.includes('@'))) {
            return res.status(406).send("email not string or not in email format")
        }

        //** validating password format and encrypting */
        if (!password) {
            return res.status(406).send("password in body needed")
        }
        if (typeof password != "string") {
            return res.status(406).send("password not string")
        }
        if (password.length < 8) {
            return res.status(406).send("password minimum is 8 character")
        }
        if (password.length > 12) {
            return res.status(406).send("password maximum is 12 character")
        }
        password = await cryptHelper.encryptPass(password)

        //** send data to db and return the common user data*/
        let userData;
        await db.users.create({ username, email, password })
            .then(result => {
                const { id, username, email } = result.get({ plain: true })
                userData = { id, username, email }
            })
            .catch(err => {
                if (err.original.code == "ER_DUP_ENTRY") {
                    return res.status(406).send(err.errors[0].message.replace('users.', ''))
                }
                return res.status(500).send("Database error")
            })

        //** create tokenization data */
        userData.token = jwt.sign(userData, process.env.JWT_SECRET_TOKEN, {
            expiresIn: '1h'
        })

        //** return response if all process is ok*/
        res.status(200).json(userData)
    },
    logout: async function (req, res) {
        //** validating req body */
        if (!Object.keys(req.body).length) {
            return res.sendStatus(400)
        }

        let { token } = req.body
        //** validating username */
        if (!token) {
            return res.status(406).send("token in body needed")
        }

        //** try balcklisted token when user try to logout */
        await db.tokens.create({ blacklisted: token })
            .catch(err => {
                console.log('Catch tokens DB Error: ', err);
                return res.status(500).send("Database error")
            })

        //** return response if all process is ok*/
        res.sendStatus(200)

    }
}