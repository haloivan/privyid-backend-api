const db = require("../models")

module.exports = {
    transferSameBank: async function (req, res) {
        //** validating req body */
        if (!Object.keys(req.body).length) {
            return res.sendStatus(400)
        }

        let { userId, receiverId, transferAmount, bankCode, ip, location, userAgent, author } = req.body
        //** validating userId */
        if (!userId) {
            return res.status(406).send("userId in body needed")
        }
        if (typeof userId != "number") {
            return res.status(406).send("userId not integer")
        }

        //** validating receiverId is user that receive transfer */
        if (!receiverId) {
            return res.status(406).send("receiverId in body needed")
        }
        if (typeof receiverId != "number") {
            return res.status(406).send("receiverId not integer")
        }

        //** validating transferAmount */
        if (!transferAmount) {
            return res.status(406).send("transferAmount in body needed")
        }
        if (typeof transferAmount != "number") {
            return res.status(406).send("transferAmount not integer")
        }
        transferAmount = parseInt(transferAmount)

        //** validating bankCode */
        if (!bankCode) {
            return res.status(406).send("bankCode in body needed")
        }
        if (typeof bankCode != "string") {
            return res.status(406).send("bankCode not string")
        }

        //** validating ip adress */
        if (!ip) {
            return res.status(406).send("ip in body needed")
        }
        if (typeof ip != "string") {
            return res.status(406).send("ip not string")
        }

        //** validating location */
        if (!location) {
            return res.status(406).send("location in body needed")
        }
        if (typeof location != "string") {
            return res.status(406).send("location not string")
        }

        //** validating userAgent */
        if (!userAgent) {
            return res.status(406).send("userAgent in body needed")
        }
        if (typeof userAgent != "string") {
            return res.status(406).send("userAgent not string")
        }

        //** validating author */
        if (!author) {
            return res.status(406).send("author in body needed")
        }
        if (typeof author != "string") {
            return res.status(406).send("author not string")
        }

        //** validating user data in db */
        const user = await db.users.findOne({ raw: true, where: { id: userId } })
        if (!user) {
            return res.status(404).send("User not found")
        }

        //** validating receiver as user data in db */
        const userReceiver = await db.users.findOne({ raw: true, where: { id: receiverId } })
        if (!userReceiver) {
            return res.status(404).send("User receiver not found, please check the id")
        }

        //** validating bank data in db */
        const bank = await db.bankBalances.findOne({ raw: true, where: { code: bankCode, enable: true } })
        if (!bank) {
            return res.status(404).send("Bank not found, or in disable mode")
        }

        //** validating user balance is more than balanceAchievement */
        await db.userBalances.findOne({ raw: true, where: { userId } })
            .then(result => {
                if (result.balance - transferAmount < result.balanceAchieve) {
                    return res.status(406).send("User balance is under balance achieve")
                }
            })

        //** create bank history transfer*/
        let bankHistoryTransfer;
        await db.bankBalanceHistories.create({
            bankBalanceId: bank.id,
            balanceBefore: bank.balance,
            balanceAfter: bank.balance,
            activity: `userId: ${user.id} - transfer ${transferAmount}`,
            type: 'kredit',
            ip,
            location,
            userAgent,
            author
        }).then(result => {
            //** set variable bank history if successfull */
            bankHistoryTransfer = result.get({ plain: true })
        }).catch(err => {
            //** send response 500 if catch error went crete new bank history */
            console.log('Balance history transfer error: ', err);
            return res.status(500).send("Database: error went create new bank balance history transfer")
        })

        //** create bank history receive transfer*/
        let bankHistoryReceiver;
        await db.bankBalanceHistories.create({
            bankBalanceId: bank.id,
            balanceBefore: bank.balance,
            balanceAfter: bank.balance,
            activity: `userId: ${userReceiver.id} - receive transfer ${transferAmount}`,
            type: 'debit',
            ip,
            location,
            userAgent,
            author
        }).then(result => {
            //** set variable bank history if successfull */
            bankHistoryReceiver = result.get({ plain: true })
        }).catch(err => {
            //** send response 500 if catch error went crete new bank history */
            console.log('Balance history receiver error: ', err);
            return res.status(500).send("Database: error went create new bank balance history receiver")
        })

        async function handler(userBalance, status, transferHistory) {
            console.log(userBalance);
            //** defining variable that based on status */
            let balanceAfter;
            let activity;
            let type;
            if (status == 'transfer') {
                balanceAfter = userBalance.balance - transferAmount
                activity = `transfer to userId: ${userReceiver.id} - ${transferAmount}`
                type = 'kredit'
            }
            if (status == 'receive') {
                balanceAfter = userBalance.balance + transferAmount
                activity = `receive from userId: ${user.id} - ${transferAmount}`
                type = 'debit'
            }

            let history;
            await db.userBalanceHistories.create({
                userBalanceId: userBalance.id,
                balanceBefore: userBalance.balance,
                balanceAfter,
                activity,
                type,
                ip,
                location,
                userAgent,
                author
            }).then(result => {
                //** set variable history if successfull */
                history = result.get({ plain: true })
            }).catch(async (err) => {
                //** send response 500 if catch error went crete new history and delete bank history*/
                //** then reseting bank balance */
                console.log('Balance history error: ', err);
                if (transferHistory) {
                    const lastHistory = await db.userBalanceHistories.findOne({ raw: true, where: { id: transferHistory.id } })
                    await db.userBalance.update({ balance: lastHistory.balanceBefore }, { where: { id: lastHistory.userBalanceId } }).catch(() => { })
                    await db.userBalanceHistories.destroy({ where: { id: transferHistory.id } }).catch(() => { })
                }
                await db.bankBalanceHistories.destroy({ where: { id: bankHistoryTransfer.id } }).catch(() => { })
                await db.bankBalanceHistories.destroy({ where: { id: bankHistoryReceiver.id } }).catch(() => { })
                await db.bankBalances.update({ balance: bank.balance }, { where: { id: bank.id } })
                return res.status(500).send("Database: error went create balance history")
            })

            //** updating userBalance */
            await db.userBalances.update({ balance: balanceAfter }, { where: { id: userBalance.id } })
                .catch(async (err) => {
                    //** if error delete user balance history and delete bank history */
                    //** then reseting bank balance */
                    //** and send response 500 */
                    console.log('Update user balance error: ', err);
                    await db.userBalanceHistories.destroy({ where: { id: history.id } })
                    if (transferHistory) {
                        const lastHistory = await db.userBalanceHistories.findOne({ raw: true, where: { id: transferHistory.id } })
                        await db.userBalance.update({ balance: lastHistory.balanceBefore }, { where: { id: lastHistory.userBalanceId } }).catch(() => { })
                        await db.userBalanceHistories.destroy({ where: { id: transferHistory.id } }).catch(() => { })
                    }
                    await db.bankBalanceHistories.destroy({ where: { id: bankHistoryTransfer.id } })
                    await db.bankBalanceHistories.destroy({ where: { id: bankHistoryReceiver.id } })
                    await db.bankBalances.update({ balance: bank.balance }, { where: { id: bank.id } })
                    return res.status(500).send("Database: error went update of user balance")
                })
            if (transferHistory) {
                //** if all flow sucessfull will logging the history user and send response 200 */
                console.log('transfer sucessfull: ', JSON.stringify(history));
                return res.sendStatus(200)
            }
            return history
        }
        //** catch user balance detail, if null try create new balance for user transfer*/
        let transferHistory;
        await db.userBalances.create({ userId, balance: 0, balanceAchieve: 50000 })
            .then(async (result) => {
                console.log('userBalance successfull created');
                //** if create successfull then try create topup history */
                const userBalance = result.get({ plain: true })
                transferHistory = await handler(userBalance, 'transfer')
                return
            })
            .catch(async (err) => {
                if (err.original.code == "ER_DUP_ENTRY") {
                    //** if create unsuccessfull find userBalance data then try create topup history */
                    console.log('userBalance have been created before');
                    const userBalance = await db.userBalances.findOne({ raw: true, where: { userId } })
                    transferHistory = await handler(userBalance, 'transfer')
                    return
                }
                //** if error not defined will delete bank history */
                //** and send response 500 */
                console.log('New user balance error: ', err);
                await db.bankBalanceHistories.destroy({ where: { id: bankHistoryTransfer.id } })
                await db.bankBalanceHistories.destroy({ where: { id: bankHistoryReceiver.id } })
                await db.bankBalances.update({ balance: bank.balance }, { where: { id: bank.id } })
                return res.status(500).send("Database error")
            })

        //** catch user balance detail, if null try create new balance for user receiver*/
        await db.userBalances.create({ userId: receiverId, balance: 0, balanceAchieve: 50000 })
            .then(async (result) => {
                console.log('userBalance successfull created');
                //** if create successfull then try create topup history */
                const userBalance = result.get({ plain: true })
                await handler(userBalance, 'receive', transferHistory)
                return
            })
            .catch(async (err) => {
                if (err.original.code == "ER_DUP_ENTRY") {
                    //** if create unsuccessfull find userBalance data then try create topup history */
                    console.log('userBalance have been created before');
                    const userBalance = await db.userBalances.findOne({ raw: true, where: { userId: receiverId } })
                    await handler(userBalance, 'receive', transferHistory)
                    return
                }
                //** if error not defined will delete bank history */
                //** and send response 500 */
                console.log('New user balance error: ', err);
                await db.bankBalanceHistories.destroy({ where: { id: bankHistoryTransfer.id } })
                await db.bankBalanceHistories.destroy({ where: { id: bankHistoryReceiver.id } })
                await db.bankBalances.update({ balance: bank.balance }, { where: { id: bank.id } })
                return res.status(500).send("Database error")
            })

    },
    topup: async function (req, res) {
        //** validating req body */
        if (!Object.keys(req.body).length) {
            return res.sendStatus(400)
        }

        let { userId, topupAmount, bankCode, ip, location, userAgent, author } = req.body
        //** validating userId */
        if (!userId) {
            return res.status(406).send("userId in body needed")
        }
        if (typeof userId != "number") {
            return res.status(406).send("userId not integer")
        }

        //** validating topupAmount */
        if (!topupAmount) {
            return res.status(406).send("topupAmount in body needed")
        }
        if (typeof topupAmount != "number") {
            return res.status(406).send("topupAmount not integer")
        }

        //** validating bankCode */
        if (!bankCode) {
            return res.status(406).send("bankCode in body needed")
        }
        if (typeof bankCode != "string") {
            return res.status(406).send("bankCode not string")
        }

        //** validating ip adress */
        if (!ip) {
            return res.status(406).send("ip in body needed")
        }
        if (typeof ip != "string") {
            return res.status(406).send("ip not string")
        }

        //** validating location */
        if (!location) {
            return res.status(406).send("location in body needed")
        }
        if (typeof location != "string") {
            return res.status(406).send("location not string")
        }

        //** validating userAgent */
        if (!userAgent) {
            return res.status(406).send("userAgent in body needed")
        }
        if (typeof userAgent != "string") {
            return res.status(406).send("userAgent not string")
        }

        //** validating author */
        if (!author) {
            return res.status(406).send("author in body needed")
        }
        if (typeof author != "string") {
            return res.status(406).send("author not string")
        }

        //** validating user data in db */
        const user = await db.users.findOne({ raw: true, where: { id: userId } })
        if (!user) {
            return res.status(404).send("User not found")
        }

        //** validating bank data in db */
        const bank = await db.bankBalances.findOne({ raw: true, where: { code: bankCode, enable: true } })
        if (!bank) {
            return res.status(404).send("Bank not found, or in disable mode")
        }

        //** save amount to bank data and create bank history */
        await db.bankBalances.update({ balance: bank.balance + topupAmount }, { where: { id: bank.id } })
        let bankHistory;
        await db.bankBalanceHistories.create({
            bankBalanceId: bank.id,
            balanceBefore: bank.balance,
            balanceAfter: bank.balance + topupAmount,
            activity: `userId: ${user.id} - topup ${topupAmount}`,
            type: 'debit',
            ip,
            location,
            userAgent,
            author
        }).then(result => {
            //** set variable bank history if successfull */
            bankHistory = result.get({ plain: true })
        }).catch(err => {
            //** send response 500 if catch error went crete new bank history */
            console.log('Balance history error: ', err);
            return res.status(500).send("Database: error went create new bank balance history")
        })

        async function handler(userBalance) {
            let history;
            await db.userBalanceHistories.create({
                userBalanceId: userBalance.id,
                balanceBefore: userBalance.balance,
                balanceAfter: userBalance.balance + topupAmount,
                activity: `topup ${topupAmount}`,
                type: 'debit',
                ip,
                location,
                userAgent,
                author
            }).then(result => {
                //** set variable history if successfull */
                history = result.get({ plain: true })
            }).catch(async (err) => {
                //** send response 500 if catch error went crete new history and delete bank history*/
                //** then reseting bank balance */
                console.log('Balance history error: ', err);
                await db.bankBalanceHistories.destroy({ where: { id: bankHistory.id } })
                await db.bankBalances.update({ balance: bank.balance }, { where: { id: bank.id } })
                return res.status(500).send("Database: error went create balance history")
            })

            //** updating userBalance */
            const balance = parseInt(userBalance.balance + topupAmount)
            await db.userBalances.update({ balance }, { where: { userId } })
                .catch(async (err) => {
                    //** if error delete user balance history and delete bank history */
                    //** then reseting bank balance */
                    //** and send response 500 */
                    console.log('Update user balance error: ', err);
                    await db.userBalanceHistories.destroy({ where: { id: history.id } })
                    await db.bankBalanceHistories.destroy({ where: { id: bankHistory.id } })
                    await db.bankBalances.update({ balance: bank.balance }, { where: { id: bank.id } })
                    return res.status(500).send("Database: error went update of user balance")
                })

            //** if all flow sucessfull will logging the history user and send response 200 */
            console.log('topup sucessfull: ', JSON.stringify(history));
            return res.sendStatus(200)
        }

        //** catch user balance detail, if null try create new balance for user*/
        await db.userBalances.create({ userId, balance: 0, balanceAchieve: 50000 })
            .then(async (result) => {
                console.log('userBalance successfull created');
                //** if create successfull then try create topup history */
                const userBalance = result.get({ plain: true })
                await handler(userBalance)
                return
            })
            .catch(async (err) => {
                if (err.original.code == "ER_DUP_ENTRY") {
                    //** if create unsuccessfull find userBalance data then try create topup history */
                    console.log('userBalance have been created before');
                    const userBalance = await db.userBalances.findOne({ raw: true, where: userId })
                    await handler(userBalance)
                    return
                }
                //** if error not defined will delete bank history */
                //** and send response 500 */
                console.log('New user balance error: ', err);
                await db.bankBalanceHistories.destroy({ where: { id: bankHistory.id } })
                await db.bankBalances.update({ balance: bank.balance }, { where: { id: bank.id } })
                return res.status(500).send("Database error")
            })
    }
}