const express = require('express')
const userBalanceController = require('../controllers/userBalanceController')
const passport = require('../middlewares/authMiddleware')
const tokenBlacklistedMiddleware = require('../middlewares/tokenBlacklistedMiddleware')

const app = express.Router()

app.post('/user/transfer', passport.authenticate('bearer', { session: false }), tokenBlacklistedMiddleware, async (req, res) => {
    await userBalanceController.transferSameBank(req, res)
})

module.exports = app
