const express = require('express')
const usersController = require('../controllers/usersController')

const app = express.Router()

app.post('/user/register', async (req, res) => {
    await usersController.register(req, res)
})

module.exports = app
