const express = require('express')
const usersController = require('../controllers/usersController')

const app = express.Router()

app.post('/user/login', async (req, res) => {
    await usersController.login(req, res)
})

module.exports = app
