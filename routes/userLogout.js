const express = require('express')
const usersController = require('../controllers/usersController')

const app = express.Router()

app.post('/user/logout', async (req, res) => {
    await usersController.logout(req, res)
})

module.exports = app
