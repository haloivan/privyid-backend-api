## PrivyID Pretest Backend

Ini adalah dokumentasi penjelasan singkat terkait dengan project yang ditujukan untuk recruitment PrivyID

### TECH STACK
- NodeJs 
- ExpressJS
- Sequelize
- MySQL
- JSON Web Token
- PassportJS
- Bcrypt

Project backend ini dibangun dengan bahasa pemrograman NodeJs menggunakan framework ExpressJs yang dipilih karena kemudahan pembuatan, kestabilan proses, support developer ExpressJS yang masih berjalan.
Untuk database dan ORM menggunakan MySQL dan Sequelize, dipilih karena database yang dibutuhkan kecil, dan kecepatan sequlize dalam built mempercepat pembuatan serta pemrosesan query ke framework ExpressJS.
Untuk urusan sekuritas project ini menggunakan JSON Web Token untuk menghandle authorization dan mengubah menjadi token dipadukan dengan PassportJS yang menhandle Authorization yang dikirim melalui Headers oleh user. Selain itu untuk urusan enkripsi data utamanya adalah password yang disimpan dalam database project ini memilih menggunakan bcrypt yang simple namun cukup kuat untuk merubah string password menjadi hash karakter.

### ENVIRONMENT VARIABLE
```env
NODE_ENV (development/production)

PORT
HOSTNAME (default: localhost)

DB_USERNAME
DB_PASSWORD
DB_HOST
DB_NAME
DB_NAME_TEST
(jika status NODE_ENV adalah development maka definisikan nama database untuk test)

JWT_SECRET_TOKEN
```

### INSTALLATION

- Install NodeJS (Disarankan menggunakan NVM) 
- Persiapkan environment variable
- Clone project: `git clone https://gitlab.com/haloivan/privyid-backend-api.git`
- Masuk ke folder project: `cd privyid-backend-api`
- Install dependency dan packages: `npm install`
- Migrasi Database
  
### MIGRASI DATABASE

- Install MySQL
- Install Dependency Sequelize `npm install sequelize-cli -g`
- Create database: `sequelize db:create`
- Migrasi model ke table: `sequelize db:migrate`
- Migrasi seed database: `sequelize db:seed:all`

### RUNNING PROJECT

Untuk start project secara default:

```
npm start
```

Bisa juga running menggunakan dependency tambahan yaitu PM2 agar berjalan dibackground:

- Install PM2: `npm install pm2 -g`
- Run project dengan PM2: `pm2 start server.js`
- Selebihnya tentang dokumentasi PM2 bisa dibaca di [PM2 Documentation](https://pm2.keymetrics.io/docs/usage/pm2-doc-single-page/)

### API TEST

secara default project akan dijalankan di: http://localhost:3000

#### REGISTER ROUTE

endpoint: /user/register
body:

```
{
    "username": "yourusername", (string)
    "email": "youremail@email", (string)
    "password": "yourpassword", (string length 8-12 char)
}
```

sucessfull response:

```
200 OK
```

unsucessfull response:

```
406 (Error: body format tidak sesuai)
500 (Error: Database/Server Error)
```

#### LOGIN ROUTE

endpoint: /user/login
body:

```
{
    "loginMethod": "yourusernameoremail", (string, username or email)
    "password": "yourpassword", (string length 8-12 char)
}
```

sucessfull response:

```
200 OK
{
    id: xxx
    username: xxxx
    email: xxxx
    token: xxxx
}
```

unsucessfull response:

```
406 (Error: body format tidak sesuai)
404 (Error: user data tidak ditemukan)
500 (Error: Database/Server Error)
```

#### LOGOUT ROUTE

endpoint: /user/logout
body:

```
{
    "token": "yourtoken" (string)
}
```

sucessfull response:

```
200 OK
```

unsucessfull response:

```
406 (Error: body format tidak sesuai)
500 (Error: Database/Server Error)
```

#### TOPUP ROUTE

endpoint: /user/topup
body:

```
{ 
  "userId": x, (integer)
  "topupAmount": nominal, (integer)
  "bankCode": xxx, (default: TEST)
  "ip": xxx.xxx.x.xxx, (string)
  "location": xx, (string)
  "userAgent": xxxxx, (string)
  "author": xxxxx (string) 
}
```

sucessfull response:

```
200 OK
```

unsucessfull response:

```
401 (Unathorized)
406 (Error: body format tidak sesuai)
404 (Error: Beberapa data tidak ditemukan)
500 (Error: Database/Server Error)
```

#### TRANSFER TO OTHER USER IN SAME BANK ROUTE

endpoint: /user/transfer
body:

```
{ 
  "userId": x, (integer)
  "receiverId": x, (integer, id from user as a receiver)
  "transferAmount": nominal, (integer)
  "bankCode": xxx, (default: TEST)
  "ip": xxx.xxx.x.xxx, (string)
  "location": xx, (string)
  "userAgent": xxxxx, (string)
  "author": xxxxx (string) 
}
```

sucessfull response:

```
200 OK
```

unsucessfull response:

```
401 (Unathorized)
406 (Error: body format tidak sesuai)
404 (Error: Beberapa data tidak ditemukan)
500 (Error: Database/Server Error)
```

### APP FLOW

Jika user belum register:
- User register
- User topup
- User transfer to other user
- User logout

Jika user sudah register:
- User login
- User topup
- User transfer to other user
- User logout 