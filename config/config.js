require('dotenv').config()
const { DB_USERNAME, DB_PASSWORD, DB_NAME, DB_NAME_TEST, DB_HOST } = process.env

module.exports = {
  "development": {
    "username": DB_USERNAME,
    "password": DB_PASSWORD,
    "database": DB_NAME_TEST,
    "host": DB_HOST,
    "dialect": "mysql",
    "logging": false,
    "connectTimeout": 15000

  },
  "production": {
    "username": DB_USERNAME,
    "password": DB_PASSWORD,
    "database": DB_NAME,
    "host": DB_HOST,
    "dialect": "mysql",
    "logging": false,
    "connectTimeout": 15000

  }
}
